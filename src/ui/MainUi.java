package ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import model.JuegoAritmetico;
import model.Resultado;

public class MainUi {

	private JFrame frmJuegoAritmtico;
	private JTextField tfNum1;
	private JTextField tfNum2;
	private JTextField tfNum3;
	private JTextField tfNum4;
	private JTextField tfNum5;
	private JTextField tfNum6;
	private JTextField tfNum7;
	private JTextField tfNum9;
	private JTextField tfNum10;
	private JTextField tfNum8;
	private JTextField tfNum11;
	private JTextField tfNum12;
	private JTextField tfNum13;
	private JTextField tfNum14;
	private JTextField tfNum15;
	private JTextField tfNum16;
	private JLabel lblResultado;
	private JLabel lblC1;
	private JLabel lblC2;
	private JLabel lblC3;
	private JLabel lblC4;
	private JLabel lblF1;
	private JLabel lblF2;
	private JLabel lblF3;
	private JLabel lblF4;

	private JPanel panelVacio;
	private JPanel panelJuego;
	private JRadioButton rdbtnTamanio4;

	private JuegoAritmetico _juego;
	private Resultado _resultadoSolucion;
	private List<JTextField> _inputsJr;
	private List<Integer> _datosJr;
	private int _tamanioActualElegido;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainUi window = new MainUi();
					window.frmJuegoAritmtico.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainUi() {
		initialize();
		llenarListaDeInputs();

		// Por default el juego sera de tamanio 4
		_juego = new JuegoAritmetico();
		_tamanioActualElegido = Integer.parseInt(rdbtnTamanio4.getName());
		_juego.setTamanioGrilla(_tamanioActualElegido);

		try {
			asignarNumerosAresolver(_juego.generarNumerosAresolver());
		} catch (Exception e) {
			mostrarMensaje(e.getMessage(), JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmJuegoAritmtico = new JFrame();
		frmJuegoAritmtico.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 12));
		frmJuegoAritmtico.getContentPane().setBackground(new Color(255, 255, 255));
		frmJuegoAritmtico.setTitle("Juego aritmetico");
		frmJuegoAritmtico.setBounds(100, 100, 525, 532);
		frmJuegoAritmtico.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel lblTitle = new JLabel("Juego Aritmetico");
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 20));

		panelJuego = new JPanel();
		panelJuego.setBorder(null);
		panelJuego.setBackground(new Color(255, 255, 255));

		JButton btnVerifiSolucion = new JButton("Verificar solucion");
		btnVerifiSolucion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				restablecerEstadoDeInputs();

				if (datosInputsSonCorrectos()) {
					_datosJr = obtenerDatosInputs();

					try {
						_resultadoSolucion = _juego.verificarSolucion(_datosJr);
					} catch (Exception e1) {
						mostrarMensaje(e1.getMessage(), JOptionPane.ERROR_MESSAGE);
					}

					mostrarResultadoDeSolucion(_resultadoSolucion);
				}
				else {
					mostrarMensaje("Solo se admiten numeros", JOptionPane.WARNING_MESSAGE);
				}

			}

			private void mostrarResultadoDeSolucion(Resultado resultado) {
				Color verde = new Color(50, 205, 50);
				Color rojo = new Color(220, 20, 60);

				int indexInputsJr = 0;
				for (List<Boolean> fila : resultado.getMatriz()) {
					for (int i = 0; i < fila.size(); i++) {
						boolean cacillaCorrecta = fila.get(i);

						if (cacillaCorrecta)
							_inputsJr.get(indexInputsJr).setBackground(verde);
						else
							_inputsJr.get(indexInputsJr).setBackground(rojo);

						indexInputsJr++;
					}
				}

				if (resultado.isSolucionCorrecta())
					lblResultado.setText("Resolviste la grilla!! :D");
				else
					lblResultado.setText("Hay filas y/o columnas incorrectas :/");
			}

			private boolean datosInputsSonCorrectos() {
				boolean datosSonCorrectos = true;

				for (int i = 0; i < _inputsJr.size(); i++) {
					JTextField input = _inputsJr.get(i);
					if (noEsUnDatoValido(input.getText())) {
						input.setBackground(new Color(220, 20, 60));
						datosSonCorrectos = false;
					}
				}
				return datosSonCorrectos;
			}

			private boolean noEsUnDatoValido(String dato) {
				try {
					Integer.parseInt(dato);
				} catch (NumberFormatException e) {
					return true;
				}
				return false;
			}

			private List<Integer> obtenerDatosInputs() {
				List<Integer> datos = new ArrayList<>();

				for (int i = 0; i < _inputsJr.size(); i++) {
					JTextField input = _inputsJr.get(i);
					int dato = Integer.parseInt(input.getText());

					datos.add(dato);
				}
				return datos;
			}

		});
		btnVerifiSolucion.setFont(new Font("Tahoma", Font.PLAIN, 16));

		rdbtnTamanio4 = new JRadioButton("4 x 4");
		rdbtnTamanio4.setName("4");
		rdbtnTamanio4.setEnabled(false);
		rdbtnTamanio4.setSelected(true);
		rdbtnTamanio4.setBackground(new Color(255, 255, 255));
		rdbtnTamanio4.setFont(new Font("Tahoma", Font.PLAIN, 12));

		JLabel lblNewLabel_2 = new JLabel("Tamanio de grilla");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));

		lblResultado = new JLabel("");
		lblResultado.setHorizontalAlignment(SwingConstants.CENTER);
		lblResultado.setBackground(new Color(255, 255, 255));
		lblResultado.setForeground(new Color(0, 0, 0));
		lblResultado.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JButton btnReiniciar = new JButton("Reiniciar");
		btnReiniciar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				_juego.reiniciar();

				try {
					asignarNumerosAresolver(_juego.generarNumerosAresolver());
				} catch (Exception e2) {
					mostrarMensaje(e2.getMessage(), JOptionPane.ERROR_MESSAGE);
				}

				_juego.setTamanioGrilla(_tamanioActualElegido);

				restablecerEstadoDeInputs();
				limpiarTextoInputs();
				lblResultado.setText("");
			}

			private void limpiarTextoInputs() {
				for (int i = 0; i < _inputsJr.size(); i++)
					_inputsJr.get(i).setText("");
			}
		});
		btnReiniciar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GroupLayout groupLayout = new GroupLayout(frmJuegoAritmtico.getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(lblTitle, GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
						.addContainerGap())
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
								.createSequentialGroup().addGap(10).addComponent(rdbtnTamanio4).addGap(60)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(lblResultado, GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
										.addGroup(groupLayout.createSequentialGroup().addGap(49)
												.addComponent(btnVerifiSolucion, GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addGap(48))
										.addGroup(groupLayout.createSequentialGroup()
												.addPreferredGap(ComponentPlacement.RELATED, 90, GroupLayout.PREFERRED_SIZE)
												.addComponent(btnReiniciar).addGap(85)))
								.addGap(126))
								.addGroup(groupLayout.createSequentialGroup().addComponent(lblNewLabel_2)
										.addContainerGap(442, Short.MAX_VALUE))))
				.addGroup(groupLayout.createSequentialGroup().addGap(107)
						.addComponent(panelJuego, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(152, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(lblTitle, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(panelJuego, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
						.addGap(27)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addComponent(lblNewLabel_2)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(rdbtnTamanio4))
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(lblResultado, GroupLayout.PREFERRED_SIZE, 23,
												GroupLayout.PREFERRED_SIZE)
										.addGap(18).addComponent(btnVerifiSolucion).addGap(7)
										.addComponent(btnReiniciar)))
						.addGap(11)));

		panelJuego.setLayout(new GridLayout(5, 5, 2, 2));

		panelVacio = new JPanel();
		panelVacio.setBorder(null);
		panelVacio.setBackground(Color.WHITE);
		panelJuego.add(panelVacio);

		lblC1 = new JLabel("-");
		lblC1.setBackground(new Color(255, 255, 255));
		lblC1.setHorizontalAlignment(SwingConstants.CENTER);
		lblC1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panelJuego.add(lblC1);

		lblC2 = new JLabel("-");
		lblC2.setHorizontalAlignment(SwingConstants.CENTER);
		lblC2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panelJuego.add(lblC2);

		lblC3 = new JLabel("-");
		lblC3.setHorizontalAlignment(SwingConstants.CENTER);
		lblC3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panelJuego.add(lblC3);

		lblC4 = new JLabel("-");
		lblC4.setHorizontalAlignment(SwingConstants.CENTER);
		lblC4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panelJuego.add(lblC4);

		lblF1 = new JLabel("-");
		lblF1.setHorizontalAlignment(SwingConstants.CENTER);
		lblF1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panelJuego.add(lblF1);

		tfNum1 = new JTextField();
		tfNum1.setName("");
		tfNum1.setBackground(Color.WHITE);
		tfNum1.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum1);
		tfNum1.setColumns(3);

		tfNum2 = new JTextField();
		tfNum2.setBackground(Color.WHITE);
		tfNum2.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum2);
		tfNum2.setColumns(3);

		tfNum3 = new JTextField();
		tfNum3.setBackground(Color.WHITE);
		tfNum3.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum3);
		tfNum3.setColumns(3);

		tfNum4 = new JTextField();
		tfNum4.setBackground(Color.WHITE);
		tfNum4.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum4);
		tfNum4.setColumns(3);

		lblF2 = new JLabel("-");
		lblF2.setHorizontalAlignment(SwingConstants.CENTER);
		lblF2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panelJuego.add(lblF2);

		tfNum5 = new JTextField();
		tfNum5.setBackground(Color.WHITE);
		tfNum5.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum5.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum5);
		tfNum5.setColumns(3);

		tfNum6 = new JTextField();
		tfNum6.setBackground(Color.WHITE);
		tfNum6.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum6.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum6);
		tfNum6.setColumns(3);

		tfNum7 = new JTextField();
		tfNum7.setBackground(Color.WHITE);
		tfNum7.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum7.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum7);
		tfNum7.setColumns(3);

		tfNum8 = new JTextField();
		tfNum8.setBackground(Color.WHITE);
		tfNum8.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum8.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum8);
		tfNum8.setColumns(3);

		lblF3 = new JLabel("-");
		lblF3.setHorizontalAlignment(SwingConstants.CENTER);
		lblF3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panelJuego.add(lblF3);

		tfNum9 = new JTextField();
		tfNum9.setBackground(Color.WHITE);
		tfNum9.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum9.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum9);
		tfNum7.setColumns(3);

		tfNum10 = new JTextField();
		tfNum10.setBackground(Color.WHITE);
		tfNum10.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum10.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum10);
		tfNum7.setColumns(3);

		tfNum11 = new JTextField();
		tfNum11.setBackground(Color.WHITE);
		tfNum11.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum11.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum11);
		tfNum11.setColumns(3);

		tfNum12 = new JTextField();
		tfNum12.setBackground(Color.WHITE);
		tfNum12.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum12.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum12);
		tfNum12.setColumns(3);

		lblF4 = new JLabel("-");
		lblF4.setHorizontalAlignment(SwingConstants.CENTER);
		lblF4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panelJuego.add(lblF4);

		tfNum13 = new JTextField();
		tfNum13.setBackground(Color.WHITE);
		tfNum13.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum13.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum13);
		tfNum13.setColumns(3);

		tfNum14 = new JTextField();
		tfNum14.setBackground(Color.WHITE);
		tfNum14.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum14.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum14);
		tfNum14.setColumns(3);

		tfNum15 = new JTextField();
		tfNum15.setBackground(Color.WHITE);
		tfNum15.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum15.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum15);
		tfNum15.setColumns(3);

		tfNum16 = new JTextField();
		tfNum16.setBackground(Color.WHITE);
		tfNum16.setHorizontalAlignment(SwingConstants.CENTER);
		tfNum16.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelJuego.add(tfNum16);
		tfNum16.setColumns(3);

		groupLayout.setAutoCreateGaps(true);
		frmJuegoAritmtico.getContentPane().setLayout(groupLayout);
	}

	private void asignarNumerosAresolver(List<Integer> numeros) {
		int indexNumero = 0;

		for (int i = 0; i < panelJuego.getComponentCount(); i++) {
			Component component = panelJuego.getComponent(i);

			// La asignacion de los numeros seran primero en las columnas
			if (component instanceof JLabel) {
				JLabel label = (JLabel) component;
				label.setText(numeros.get(indexNumero).toString());
				indexNumero++;
			}
		}
	}

	private void llenarListaDeInputs() {
		_inputsJr = new ArrayList<>();

		for (int i = 0; i < panelJuego.getComponentCount(); i++) {
			Component component = panelJuego.getComponent(i);

			if (component instanceof JTextField) {
				JTextField input = (JTextField) component;
				_inputsJr.add(input);
			}
		}
	}

	private void restablecerEstadoDeInputs() {
		for (int i = 0; i < _inputsJr.size(); i++) {
			_inputsJr.get(i).setBackground(Color.WHITE);
		}
	}

	private void mostrarMensaje(String msg, int tipoMensaje) {
		JOptionPane.showMessageDialog(frmJuegoAritmtico, msg, "Ventana", tipoMensaje);
	}
}
