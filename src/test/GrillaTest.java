package test;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import model.Grilla;
import utilities.Datos;

public class GrillaTest {

	private Grilla _grilla;
	private List<Integer> _datos;
	private final int _TAMANIO = 4;

	//Por defecto los valores ya no son ceros
	
	@Before
	public void setUp() {
	}

	@Test
	public void establecerDatosAlaGrilla() {
		_datos = DatosParaTest.listaCorrectaDeNumerosParaGrilla();
		_grilla = new Grilla(_datos, _TAMANIO);
		
		int indexDatos = 0;
		for (List<Integer> fila : _grilla.getGrilla()) {
			for (Integer dato : fila) {
				int numDatos = _datos.get(indexDatos);
				int numGrilla = dato;
				
				assertTrue(numGrilla == numDatos);

				indexDatos++;
			}
		}
	}

	@Test
	public void cumpleIREP() {
		_datos = DatosParaTest.listaCorrectaDeNumerosParaGrilla();
		_grilla = new Grilla(_datos, _TAMANIO);
	}

	// Exceptions
	@Test(expected = IllegalArgumentException.class)
	public void noCumpleIREP() {
		_datos = DatosParaTest.listaTamanioIncorrectoDeNumerosParaGrilla();
		_grilla = new Grilla(_datos, _TAMANIO);
	}

}
