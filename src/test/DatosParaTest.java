package test;

import java.util.Arrays;
import java.util.List;

public class DatosParaTest {
	
	public static List<Integer> listaCorrectaDeNumerosParaColumnasYfilas() {
		return Arrays.asList(7, 9, 9, 6, 5, 7, 12, 7);
	}

	public static List<Integer> listaCorrectaDeNumerosParaGrilla() {
		return Arrays.asList(2, 1, 1, 1, 1, 2, 3, 1, 2, 4, 3, 3, 2, 2, 2, 1);
	}

	public static List<Integer> listaIncorrectaDeNumerosParaGrilla() {
		return Arrays.asList(1, 2, 2, 2, 1, 2, 3, 1, 2, 4, 3, 3, 2, 2, 2, 1);
	}
	
	public static List<Integer> listaTamanioIncorrectoDeNumerosParaGrilla() {
		return Arrays.asList(2, 1, 1, 1, 1, 2, 3, 1, 2, 4, 3, 3, 2, 2, 2, 1, 1);
	}
}
