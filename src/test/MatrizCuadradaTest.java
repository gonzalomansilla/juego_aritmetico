package test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import model.MatrizCuadrada;
import utilities.Datos;

public class MatrizCuadradaTest {

	private MatrizCuadrada<Integer> _matrizInteger;
	private final int _TAMANIO = 4;

	@Before
	public void setUp() {
		_matrizInteger = new MatrizCuadrada<>(_TAMANIO);
	}

	@Test
	public void insertarEnFilaYcolumna() {
		int datoRandom = Datos.generarNumeroEntre(1, 10);
		int pos = _TAMANIO - 1;
		_matrizInteger.establecerValorEnFilaYcolumna(pos, pos, datoRandom);

		int datoMatriz = _matrizInteger.getMatriz().get(pos).get(pos);
		
		assertTrue(datoMatriz == datoRandom);
	}
	
	@Test
	public void consultarFilaYcolumna() {
		int datoRandom = Datos.generarNumeroEntre(1, 10);
		int pos = _TAMANIO - 1;
		_matrizInteger.getMatriz().get(pos).set(pos, datoRandom);;

		assertTrue(_matrizInteger.consultarFilaYcolumna(pos, pos) == datoRandom);
	}
	
	@Test
	public void insertarUnicoValorEnColumnaDeIntegers() {
		int datoRandom = Datos.generarNumeroEntre(1, 10);
		_matrizInteger.establecerUnicoValorEnColumna(0, datoRandom);

		for (int dato : _matrizInteger.consultarColumna(0))
			assertTrue(dato == datoRandom);
	}

	@Test
	public void insertarUnicoValorEnFilaDeIntegers() {
		int datoRandom = Datos.generarNumeroEntre(1, 10);
		_matrizInteger.establecerUnicoValorEnFila(0, datoRandom);

		for (int dato : _matrizInteger.consultarFila(0))
			assertTrue(dato == datoRandom);

	}

	@Test
	public void generarMatrizDeIntegersVacia() {
		for (List<Integer> fila : _matrizInteger.getMatriz()) {
			for (Integer dato : fila)
				assertTrue(dato == null);
		}
	}

	@Test
	public void consultarColumnaDeIntegers() {
		List<Integer> columna = _matrizInteger.consultarColumna(0);

		assertTrue(columna.size() == _TAMANIO);

		for (Integer dato : columna)
			assertTrue(dato == null);
	}

	@Test
	public void consultarFilaDeIntegers() {
		List<Integer> fila = _matrizInteger.consultarFila(0);

		assertTrue(fila.size() == _TAMANIO);
		for (Integer dato : fila)
			assertTrue(dato == null);
	}

}
