package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import model.JuegoAritmetico;
import model.Resultado;

public class JuegoAritmeticoTest {

	private JuegoAritmetico _juego;

	@Before
	public void setUp() {
		_juego = new JuegoAritmetico();
	}

	@Test
	public void verificarSolucionIncorrecta() {
		_juego.set_numerosAresolver(DatosParaTest.listaCorrectaDeNumerosParaColumnasYfilas());
		_juego.setTamanioGrilla(4);

		// Primeros 4 digitos(primer fila) son incorrectos
		Resultado resul = _juego.verificarSolucion(DatosParaTest.listaIncorrectaDeNumerosParaGrilla());

		for (List<Boolean> fila : resul.getMatriz()) {
			for (Boolean dato : fila)
				assertFalse(dato);
		}
	}

	@Test
	public void verificarSolucionCorrecta() {
		_juego.set_numerosAresolver(DatosParaTest.listaCorrectaDeNumerosParaColumnasYfilas());

		_juego.setTamanioGrilla(4);

		Resultado resul = _juego.verificarSolucion(DatosParaTest.listaCorrectaDeNumerosParaGrilla());

		for (List<Boolean> fila : resul.getMatriz()) {
			for (Boolean dato : fila)
				assertTrue(dato);
		}
	}

	@Test
	public void generarNumerosAresolver() {
		_juego.setTamanioGrilla(4);
		List<Integer> numeros = _juego.generarNumerosAresolver();

		assertTrue(numeros.size() == (_juego.getTamanioGrilla() * 2));

		for (Integer n : numeros)
			assertTrue(n > 0);
	}

	// Exceptions
	@Test(expected = IllegalArgumentException.class)
	public void tamanioInvalido() {
		_juego.setTamanioGrilla(0);
		_juego.generarNumerosAresolver();
	}

}
