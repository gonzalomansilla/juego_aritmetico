package model;

import java.util.ArrayList;
import java.util.List;

public class MatrizCuadrada<T> {

	private List<List<T>> _matriz;
	private int _tamanio;

	public MatrizCuadrada(int tamanio) {
		_tamanio = tamanio;
		generarMatrizVacia();
	}

	private void generarMatrizVacia() {
		_matriz = new ArrayList<List<T>>(_tamanio);

		for (int i = 0; i < _tamanio; i++)
			_matriz.add(new ArrayList<T>(_tamanio));

		// Asignacion de Nulls
		for (List<T> fila : _matriz) {
			for (int i = 0; i < _tamanio; i++) 
				fila.add(null);
		}
	}

	public T consultarFilaYcolumna(int fila, int columna) {
		return consultarFila(fila).get(columna);
	}
	
	public List<T> consultarFila(int indexFila) {
		return _matriz.get(indexFila);
	}

	public List<T> consultarColumna(int indexColumna) {
		List<T> columna = new ArrayList<>();
		
		for (int f = 0; f < _matriz.size(); f++) {
			T dato = _matriz.get(f).get(indexColumna);
			columna.add(dato);
		}

		return columna;
	}

	public void establecerUnicoValorEnColumna(int columna, T valor) {
		for (List<T> fila : _matriz)
			fila.set(columna, valor);
	}

	public void establecerUnicoValorEnFila(int fila, T valor) {
		for (int c = 0; c < _matriz.get(fila).size(); c++)
			_matriz.get(fila).set(c, valor);
	}

	public void establecerValorEnFilaYcolumna(int fila, int columna, T valor) {
		_matriz.get(fila).set(columna, valor);
	}

	//Gets
	public List<List<T>> getMatriz() {
		return _matriz;
	}
	
	public int getTamanio() {
		return _tamanio;
	}

}
