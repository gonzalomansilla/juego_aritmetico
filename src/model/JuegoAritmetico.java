package model;

import java.util.ArrayList;
import java.util.List;

import utilities.Datos;

public class JuegoAritmetico {

	private final int NUMERO_MAXIMO = 9;
	private final int NUMERO_MINIMO = 1;

	private List<Integer> _datosJr;
	private List<Integer> _numerosAresolver;

	private int _tamanioGrilla;
	private Resultado _resultado;
	private Grilla _grilla;

	public JuegoAritmetico() {
	}

	public Resultado verificarSolucion(List<Integer> listaDatos) {
		establecerDatos(listaDatos);

		verificar();

		establecerSiEsSolucion();

		return _resultado;
	}

	private void establecerDatos(List<Integer> listaDatos) {
		_resultado = new Resultado(_tamanioGrilla);
		_grilla = new Grilla(listaDatos, _tamanioGrilla);
		_datosJr = listaDatos;
	}

	private void verificar() {
		for (int i = 0; i < _tamanioGrilla; i++) {
			comprobarColumna(i);
			comprobarFila(i);
		}

	}

	private void establecerSiEsSolucion() {
		for (List<Boolean> fila : _resultado.getMatriz()) {
			for (Boolean dato : fila) {
				if (dato == false)
					_resultado.setSolucionCorrecta(false);

				return;
			}
		}

		_resultado.setSolucionCorrecta(true);
	}

	private void comprobarFila(int indexfila) {
		// Calculo para obtener el indice del numero a resolver de l afila
		// correspondiente
		int indexListaNumerosParaFila = indexfila * 2 + (4 - indexfila);

		int numeroAresolver = _numerosAresolver.get(indexListaNumerosParaFila);
		List<Integer> fila = _grilla.consultarFila(indexfila);

		int sumaFila = sumarNumerosDeLista(fila);

		if (sumaFila == numeroAresolver)
			_resultado.establecerUnicoValorEnFilaConservandoFalse(indexfila, true);
		else
			_resultado.establecerUnicoValorEnFila(indexfila, false);

	}

	private void comprobarColumna(int indexColumna) {
		int indexListaNumerosParaColumna = indexColumna;

		int numeroAresolver = _numerosAresolver.get(indexListaNumerosParaColumna);
		List<Integer> columna = _grilla.consultarColumna(indexColumna);

		int sumaColumna = sumarNumerosDeLista(columna);

		if (sumaColumna == numeroAresolver)
			_resultado.establecerUnicoValorEnColumnaConservandoFalse(indexColumna, true);
		else
			_resultado.establecerUnicoValorEnColumna(indexColumna, false);
	}

	private int sumarNumerosDeLista(List<Integer> lista) {
		int suma = 0;

		for (Integer dato : lista)
			suma += dato;

		return suma;
	}

	public void reiniciar() {
		_grilla = null;
		_resultado = null;
		_datosJr = null;
		_numerosAresolver = null;
	}

	public List<Integer> generarNumerosAresolver() {
		if (_tamanioGrilla <= 0)
			throw new IllegalArgumentException("El tamanio no fue asignada o no es correcta");

		List<Integer> resul = new ArrayList<Integer>();

		// Primera mitad corresponden a las filas y el resto a las columnas
		int cantNumeros = _tamanioGrilla * 2;
		for (int i = 0; i < cantNumeros; i++) {
			int numero = Datos.generarNumeroEntre(NUMERO_MINIMO, NUMERO_MAXIMO);
			resul.add(numero);
		}

		_numerosAresolver = new ArrayList<Integer>(resul);
		return _numerosAresolver;
	}

	// Gets & Sets
	public List<Integer> getDatosJr() {
		return _datosJr;
	}

	public int getTamanioGrilla() {
		return _tamanioGrilla;
	}

	public List<List<Boolean>> getResultado() {
		return _resultado.getMatriz();
	}

	public void setTamanioGrilla(int tamanio) {
		_tamanioGrilla = tamanio;
	}

	public void set_numerosAresolver(List<Integer> _numerosAresolver) {
		this._numerosAresolver = _numerosAresolver;
	}

}
