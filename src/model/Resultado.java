package model;

import java.util.ArrayList;
import java.util.List;

public class Resultado extends MatrizCuadrada<Boolean> {

	private boolean solucionCorrecta;

	public Resultado(int tamanio) {
		super(tamanio);
	}

	public List<List<Boolean>> getResultado() {
		return new ArrayList<List<Boolean>>(getMatriz());
	}

	public void establecerUnicoValorEnColumnaConservandoFalse(int columna, boolean valor) {
		for (int f = 0; f < this.getTamanio(); f++) {
			Boolean valorCeldaDeColumna = this.consultarFilaYcolumna(f, columna);

			if (valorCeldaDeColumna == null || valorCeldaDeColumna == true)
				this.establecerValorEnFilaYcolumna(f, columna, valor);
		}
	}

	public void establecerUnicoValorEnFilaConservandoFalse(int fila, boolean valor) {
		for (int c = 0; c < this.getTamanio(); c++) {
			Boolean valorCeldaDeFila = this.consultarFilaYcolumna(fila, c);

			if (valorCeldaDeFila == null || valorCeldaDeFila == true)
				this.establecerValorEnFilaYcolumna(fila, c, valor);
		}
	}

	public void setSolucionCorrecta(boolean solucionCorrecta) {
		this.solucionCorrecta = solucionCorrecta;
	}

	public boolean isSolucionCorrecta() {
		return solucionCorrecta;
	}

}
