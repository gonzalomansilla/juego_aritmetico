package model;

import java.util.ArrayList;
import java.util.List;

public class Grilla extends MatrizCuadrada<Integer> {

	public Grilla(List<Integer> lista, int tamanio) {
		super(tamanio);

		if (cumpleIREP(lista, tamanio))
			establecerDatosAgrilla(lista);
	}

	private void establecerDatosAgrilla(List<Integer> lista) {
		int indexLista = 0;

		for (int f = 0; f < getTamanio(); f++) {
			for (int c = 0; c < getTamanio(); c++) {
				int dato = lista.get(indexLista);
				establecerValorEnFilaYcolumna(f, c, dato);

				indexLista++;
			}
		}
	}

	private boolean cumpleIREP(List<Integer> lista, int tamanio) {
		if (lista.size() % tamanio != 0)
			throw new IllegalArgumentException(
					"El tama�o de la grilla debe ser multiplo del tama�o de la lista");

		return true;
	}

	// GETs
	public List<List<Integer>> getGrilla() {
		return new ArrayList<List<Integer>>(getMatriz());
	}
}
