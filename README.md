## Juego Aritmético
El juego consta de una grilla de 4x4, con de una fila y columna con distintos números donde el jugador deberá ingresar la cantidad justa en cada celda, para que al sumarlas den el número generado en su respectiva fila/columna

Características más importantes
- Generación automática de los números
- Se colorea las filas o columnas correctas e incorrectas
- Verificación de lo ingresado por el usuario (Regex)